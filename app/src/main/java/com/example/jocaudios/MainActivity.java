package com.example.jocaudios;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button comenzar;
    private TextView titulo;
    private ImageButton play;
    private ImageButton resp1;
    private ImageButton resp2;
    private ImageButton resp3;
    private ArrayList<Integer> numeros = new ArrayList<Integer>();
    private int contador = 0;
    private ArrayList<Integer> animales = new ArrayList<Integer>();
    private ArrayList<Integer> respuestas = new ArrayList<Integer>();
    private int respuesta = 0;
    private int animal1;
    private int animal2;
    private int animal3;
    private MediaPlayer mp = new MediaPlayer();
    private Integer auxiliar1;
    private int sonidofinal = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        comenzar = findViewById(R.id.comenzar);
        titulo = findViewById(R.id.titulo);
        play = findViewById(R.id.play);
        resp1 = findViewById(R.id.resp1);
        resp2 = findViewById(R.id.resp2);
        resp3 = findViewById(R.id.resp3);



        comenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titulo.setVisibility(View.VISIBLE);
                comenzar.setVisibility(View.INVISIBLE);
                play.setVisibility(View.VISIBLE);
                resp1.setVisibility(View.VISIBLE);
                resp2.setVisibility(View.VISIBLE);
                resp3.setVisibility(View.VISIBLE);
                titulo.setText("Ronda: "+((respuestas.size())+1)+"/5");

                sonidofinal = 0;
                numeros.add(1);
                numeros.add(2);
                numeros.add(3);
                numeros.add(4);
                numeros.add(5);
                Collections.shuffle(numeros);
                respuesta = 0;
                prepararpregunta(numeros);

            }
        });
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mp.isPlaying()) {
                switch (respuesta){
                    case 1:
                            mp = MediaPlayer.create(MainActivity.this, R.raw.bat1);
                        break;
                    case 2:
                           mp = MediaPlayer.create(MainActivity.this, R.raw.bear1);
                        break;
                    case 3:
                           mp = MediaPlayer.create(MainActivity.this, R.raw.elephantcub);
                        break;
                    case 4:
                              mp = MediaPlayer.create(MainActivity.this, R.raw.lioncub2);
                        break;
                    case 5:
                        mp = MediaPlayer.create(MainActivity.this, R.raw.rmonkeycolobus);
                        break;
                }

                    mp.start();
                }
            }
        });

        resp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(respuestas.size()==5)) {
                    if (animal1 == respuesta) {
                        Collections.shuffle(numeros);
                        respuesta = 0;
                        prepararpregunta(numeros);
                        titulo.setText("Ronda: "+(respuestas.size())+"/5");
                    }
                } else {
                    finale();
                }
            }
        });
        resp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(respuestas.size()==5)) {
                    if (animal2 == respuesta) {
                        Collections.shuffle(numeros);
                        respuesta = 0;
                        prepararpregunta(numeros);
                        titulo.setText("Ronda: "+(respuestas.size())+"/5");
                    }
                }else {
                    finale();
                }
            }
        });
        resp3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(respuestas.size()==5)) {
                    if (animal3 == respuesta) {
                        Collections.shuffle(numeros);
                        respuesta = 0;
                        prepararpregunta(numeros);
                        titulo.setText("Ronda: "+(respuestas.size())+"/5");
                    }
                }else {
                    finale();
                }
            }
        });



    }

    public void finale(){
        resp2.setImageResource(R.drawable.animales);
        titulo.setVisibility(View.VISIBLE);
        comenzar.setVisibility(View.VISIBLE);
        play.setVisibility(View.INVISIBLE);
        resp1.setVisibility(View.INVISIBLE);
        resp2.setVisibility(View.VISIBLE);
        resp3.setVisibility(View.INVISIBLE);
        titulo.setText("Felicitats!");
        if (sonidofinal==0) {
            mp = MediaPlayer.create(MainActivity.this, R.raw.ff9win);
            mp.start();
            sonidofinal = 1;
        }
    }

    public void prepararpregunta(List<Integer> numeros){
        respuesta = 0;
        animales= new ArrayList<Integer>();
        while (animales.size()<3) {
            if (!(respuestas.contains(numeros.get(0)))) {
                auxiliar1 = numeros.get(0);
                animales.add(auxiliar1);
                if (respuesta == 0) {
                    respuestas.add(numeros.get(0));
                    respuesta = numeros.get(0);
                }
                animales.add(numeros.get(1));
                animales.add(numeros.get(2));
            }
            Collections.shuffle(numeros);
        }
        // tenemos 3 numeros diferentes aleatorios en animales el 0 es el correcto y en respuesta está tambien
      //  Collections.shuffle(animales);
        animal1 = animales.get(0);
        switch (animales.get(0)){
            case 1:
                       resp1.setImageResource(R.drawable.bat);
                break;
            case 2:
                       resp1.setImageResource(R.drawable.bear);
                break;
            case 3:
                       resp1.setImageResource(R.drawable.elephant);
                break;
            case 4:
                       resp1.setImageResource(R.drawable.lion);
                break;
            case 5:
                       resp1.setImageResource(R.drawable.monkey);
                break;
        }
        animal2 = animales.get(1);
        switch (animales.get(1)){
            case 1:
                         resp2.setImageResource(R.drawable.bat);
                break;
            case 2:
                           resp2.setImageResource(R.drawable.bear);
                break;
            case 3:
                          resp2.setImageResource(R.drawable.elephant);
                break;
            case 4:
                         resp2.setImageResource(R.drawable.lion);
                break;
            case 5:
                       resp2.setImageResource(R.drawable.monkey);
                break;
        }
        animal3 = animales.get(2);
        switch (animales.get(2)){
            case 1:
                        resp3.setImageResource(R.drawable.bat);
                break;
            case 2:
                          resp3.setImageResource(R.drawable.bear);
                break;
            case 3:
                         resp3.setImageResource(R.drawable.elephant);
                break;
            case 4:
                        resp3.setImageResource(R.drawable.lion);
                break;
            case 5:
                           resp3.setImageResource(R.drawable.monkey);
                break;
        }
        if (!mp.isPlaying()) {
        switch (respuesta) {
                case 1:
                    mp = MediaPlayer.create(MainActivity.this, R.raw.bat1);
                    break;
                case 2:
                    mp = MediaPlayer.create(MainActivity.this, R.raw.bear1);
                    break;
                case 3:
                    mp = MediaPlayer.create(MainActivity.this, R.raw.elephantcub);
                    break;
                case 4:
                    mp = MediaPlayer.create(MainActivity.this, R.raw.lioncub2);
                    break;
                case 5:
                    mp = MediaPlayer.create(MainActivity.this, R.raw.rmonkeycolobus);
                    break;
            }
            mp.start();
        }

    }

}